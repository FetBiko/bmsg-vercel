import databaseConnect from '../../../utils/databaseConnect';
import Message from '../../../models/Message'

databaseConnect();

export default async (req, res) => {
    const {
        query: { id },
        method
    } = req;

    switch( method ) {
        case 'GET':
            const message = await Message.findOneAndDelete( { secure_id: id } );

            if(!message) {
                return res.status(400).json({ success: false });
            }

            res.status(200).json({ success: true, data: message })
            break;

        case 'DELETE':

            const deletedMessage = await Message.deleteOne( { secure_id: id } );

            if(!deletedMessage) 
                return res.status(400).json( { success: false });
                
            res.status(200).json( {success: true, data: {} });

            break;
        default: 
            res.status(400).json( { success: false });
    }
}