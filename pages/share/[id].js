import styles from '../../styles/Home.module.scss'
import messageStyles from '../../components/MessageInput/MessageInput.module.scss'
import React from 'react';
import Head from 'next/head'
import Button from '../../components/Button/Button';

export default class Share extends React.Component {

  constructor( props ) {
    super(props);
    this.copy = this.copy.bind(this);
  }

  copy( e ) {
    const item = e.target;
    var r = document.createRange();
    r.selectNode(item);
    window.getSelection().removeAllRanges();
    window.getSelection().addRange(r);
    document.execCommand('copy');
    window.getSelection().removeAllRanges();
    const tempText = item.innerText;
    item.innerText = 'Copied!';

    setTimeout( () => {
      item.innerText = tempText;
    }, 2500 );
  }

  render() {
    const style = {
      copy: {
        cursor: 'poiter'
      }
    }
    return (
      <div className={styles.container}>
          <Head>
            <script dangerouslySetInnerHTML={{
                __html: `(function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)}; m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)}) (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym"); ym(67708645, "init", { clickmap:true, trackLinks:true, accurateTrackBounce:true, trackHash:true });`
            }} />
            <title>BMSG — Send self-destructing messages (beta)</title>
            <meta name="description" content="Messages will self-destruct after being read." />
          </Head>


          <div className={messageStyles.MessageInput__counter}>Click to copy link. The message will self-destruct after reading it.</div>
          <div className={messageStyles.MessageInput__input + ' ' + styles.copy} style={style.copy} onClick={this.copy}>https://bmsg.vercel.app/m/{this.props.messageId}</div>
          <Button href="/">New Message</Button> 
      </div>
    )
  }
}

Share.getInitialProps = async ( { query: { id  } }) => {

  return { messageId: id }
}