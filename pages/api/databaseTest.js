import databaseConnect from '../../utils/databaseConnect';

databaseConnect();

export default async (req, res) => {
    res.status(200).json({ test: 'See logs for details' });
};