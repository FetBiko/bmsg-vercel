import styles from './Timer.module.scss';
import React, { useEffect, useState } from 'react';

function HHMMSS( time) {
    var sec_num = parseInt(time, 10); // don't forget the second param
    var hours   = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (hours   < 10) hours   = "0" + hours;
    if (minutes < 10) minutes = "0" + minutes;
    if (seconds < 10) seconds = "0" + seconds;

    // return {
    //     hours, minutes, seconds
    // }

    if( hours === "00" )
        return minutes + ':' + seconds;

    return hours + ':' + minutes + ':' + seconds;
}
    
export default class Timer extends React.Component {
    constructor( props ) {
        super(props);

        const { lifetime, onTimerEnds } = props;

        this.state = {
            left: lifetime
        };

        this.onTimerEnds = onTimerEnds;
        this.countDown = this.countDown.bind(this);

        this.countDown();
    }

    
    countDown() {

        if( this.state.left == 0 ) {
            if( this.onTimerEnds && typeof this.onTimerEnds === "function" ) this.onTimerEnds();
            return
        }

        let left = this.state.left - 1;
        
        this.setState({ left });
        
        setTimeout( () => {
            this.countDown()
        }, 1000);
    }

    render() {
        return (
            <div className={styles.Timer}>{HHMMSS(this.state.left)}</div>  
        );
    }
  }