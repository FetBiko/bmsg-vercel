const mongoose = require('mongoose');

const MessageShema = new mongoose.Schema({
    text: {
        type: String,
        required: true,
        maxlength: [240, "Message cannot be more than 240 characters"]
    },
    secure_id: {
        type: String,
        required: true,
    },
    lifetime: {
        type: Number,
        required: true,
        min: 0,
        max: [ 60 * 60, "Message lifetime cannot be more than 1 hour (3600 seconds)"]
    }
});

module.exports = mongoose.models.Message || mongoose.model( 'Message', MessageShema );