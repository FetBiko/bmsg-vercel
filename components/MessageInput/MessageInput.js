import styles from './MessageInput.module.scss';
import React from 'react';

export default class MessageInput extends React.Component {
    constructor( props ) {
        super(props);

        const { maxlength, onChange } = props;

        this.state = {
            length: 0,
            maxlength: maxlength || 240,
            value: ""
        }

        this.handleChange = onChange;
    }
  
    onKeyPressed(event) {

        this.setState( { 
            length: event.target.innerText.length,
            value: event.target.innerText
        });

        let combination = event.ctrlKey && ['a', 'x', 'z', 'c'].includes( event.key )

        this.changed( this.state.value );

        if( this.state.length >= (this.state.maxlength - 1 ) && event.keyCode != 8 && !combination )
            event.preventDefault();
    }

    changed() {

        if( this.handleChange && typeof this.handleChange === 'function' )
            this.handleChange( this.state.value );
    }

    render() {
        const conter = this.state.length > this.state.maxlength * .75 ? `${this.state.length}/${this.state.maxlength}` : 'Your message'
        return (
            <div className={styles.MessageInput}>
                <span className={styles.MessageInput__counter}>{conter}</span>
                <div onKeyDown={(e) => this.onKeyPressed(e)} className={styles.MessageInput__input} placeholder="Write your message.." contentEditable="true"></div>
            </div>
            
        );
    }
  }