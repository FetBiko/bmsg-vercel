import styles from '../styles/Home.module.scss'
import MessageInput from '../components/MessageInput/MessageInput';
import Button from '../components/Button/Button';
import React from 'react';
import router from 'next/router';
import Head from 'next/head';

export default class Home extends React.Component {

  constructor( props ) {
    super(props);

    this.state = {
      messageText: ''
    }

    this.handleChangeInput = this.handleChangeInput.bind(this);
    this.send = this.send.bind(this);
  }

  handleChangeInput = (messageText) => this.setState({ messageText });

  async send() {
    const msg = {
      text: this.state.messageText,
      lifetime: 60
    };

    const res = await fetch('https://bmsg.vercel.app/api/messages/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json;charset=utf-8'
      },
      body: JSON.stringify( msg )
    });

    const { success, data } = await res.json();

    if( success ) {
      router.push('/share/' + data.secure_id);
    }
    
  }

  render() {
    return (
      <div className={styles.container}>
        <Head>
          <script dangerouslySetInnerHTML={{
              __html: `(function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)}; m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)}) (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym"); ym(67708645, "init", { clickmap:true, trackLinks:true, accurateTrackBounce:true, trackHash:true });`
          }} />
          <title>BMSG — Send self-destructing messages (beta)</title>
          <meta name="description" content="Messages will self-destruct after being read." />
        </Head>

        <MessageInput onChange={this.handleChangeInput} />
        <Button onClick={this.send}>Send</Button>

        <a  href="https://www.producthunt.com/posts/bmsg-send-self-destructing-messages?utm_source=badge-featured&utm_medium=badge&utm_souce=badge-bmsg-send-self-destructing-messages" target="_blank"><img className={styles.productHunt} src="https://api.producthunt.com/widgets/embed-image/v1/featured.svg?post_id=268577&theme=dark" alt="BMSG — Send self-destructing messages - Messages will self-destruct after being read. BETA. | Product Hunt" width="250" height="54" /></a>
      </div>
    )
  }
}
