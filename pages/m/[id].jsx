import styles from '../../styles/Home.module.scss'
import messageStyles from '../../components/MessageInput/MessageInput.module.scss';
import Button from '../../components/Button/Button';
import Head from 'next/head'

export default class Message extends React.Component {

  constructor( props ) {
    super(props);

    // console.log( 'props', props );
    this.send = this.send.bind(this);
  }
  
  send() {

  }

  render() {
    return (
      <div className={styles.container}>
        <Head>
          <script dangerouslySetInnerHTML={{
              __html: `(function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)}; m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)}) (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym"); ym(67708645, "init", { clickmap:true, trackLinks:true, accurateTrackBounce:true, trackHash:true });`
          }} />
          <title>BMSG — Send self-destructing messages (beta)</title>
          <meta name="description" content="Messages will self-destruct after being read." />
        </Head>

        { !this.props.success && <div className={messageStyles.MessageInput__input}>Message not found 🔎</div> }
        { this.props.success && <div className={messageStyles.MessageInput__input}> { this.props.message.text }</div> }
        <Button href="/">Reply</Button> 
      </div>
    )
  }
}

Message.getInitialProps = async ( req ) => {

  let { query: { id  } } = req;

  let userAgent = req && req.headers ? req.headers['user-agent'] : (req && req.req && req.req.headers ? req.req.headers['user-agent'] : navigator ? navigator.userAgent : '' )

  if( userAgent !== 'TelegramBot (like TwitterBot)' && userAgent !== 'Mozilla/5.0 (compatible; vkShare; +http://vk.com/dev/Share)' ) {
    const res = await fetch(`https://bmsg.vercel.app/api/messages/${id}`);
    const { data, success } = await res.json();
    return { message: { ...data }, success };
  }


  return { message: {}, success: false };
}