import React from 'react'
import Link from 'next/link'
import styles from './Button.module.scss';

export default function ButtonLink({ className, href, hrefAs, size, mode, children, prefetch, stretched, onClick }) {

    if( onClick && typeof onClick === 'function' )
        return (
            <a className={styles.Button} onClick={onClick}>{children}</a>
        )

    return (
        <Link href={href} as={hrefAs} prefetch>
            <a className={styles.Button}>
                {children}
            </a>
        </Link>
    )
}