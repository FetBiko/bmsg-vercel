import databaseConnect from '../../../utils/databaseConnect';
import Message from '../../../models/Message'

databaseConnect();

export default async (req, res) => {
  const { method } = req;

  switch( method ) {
    
    case "POST": 

        const secure_id = (Date.now() * Math.random()).toString(36).substr(0, 8).replace('.', '').toUpperCase()
        const message = await Message.create( { ...req.body, secure_id } );

        res.status(201).json({ success: true, data: message })

        console.log( 'new message', message )
      break;

    default: 
      res.status(400).json( { success: false, reason: 'Not Avaliable' });
  }
}
